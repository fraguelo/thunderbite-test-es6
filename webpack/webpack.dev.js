const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const { prod_Path, src_Path } = require("./path");
const { selectedPreprocessor } = require("./loader");

const serverPort = "8080";

module.exports = {
  entry: ["babel-polyfill", "./" + src_Path + "/index.js"],
  devtool: "source-map",
  output: {
    path: path.resolve(__dirname, prod_Path),
    filename: "[name].[chunkhash].js"
  },
  devServer: {
    host: "0.0.0.0",
    port: 8080,
    open: true,
    public: "localhost:8080",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: selectedPreprocessor.fileRegexp,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader",
            options: {
              modules: false,
              sourceMap: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true
            }
          },
          {
            loader: selectedPreprocessor.loaderName,
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: "file-loader"
          }
        ]
      },
      {
        test: /node_modules[/\\]createjs/,
        use: [
          {
            loader: "imports-loader?this=>window"
          },
          {
            loader: "exports-loader?window.createjs"
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      createjs: "createjs/builds/1.0.0/createjs.js"
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      hash: false,
      template: "./" + src_Path + "/index.html",
      filename: "index.html"
    })
  ]
};
