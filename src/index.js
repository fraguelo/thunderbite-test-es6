import "normalize.css";
import "./styles/style.scss";

import Showdown from "./modules/Showdown";
import Wheel from "./modules/Wheel";
import { resizeCanvas } from "./components/Canvas";

const main = () => {
  Showdown();
  Wheel();
  window.onresize = resizeCanvas;
  resizeCanvas();
};

main();
