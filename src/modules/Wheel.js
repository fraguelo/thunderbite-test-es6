import "isomorphic-fetch";
import Api from "easy-fetch-api";

import { Stage, Container, Bitmap, Tween, Ease } from "createjs";
import load from "../components/ResourceLoader";
import { createCanvas, tickStage } from "../components/Canvas";
import {
  setOverEvents,
  fadeIn,
  addBlur,
  removeLoop,
  removeBlur
} from "../components/Animation";

import { WHEEL_RESOURCES, RANDOM_URL_API } from "../conf/constants";

let stage;
let loader;
let container;
const deg = 360;
let spinning = false;
let wheelLoop;

const Wheel = () => {
  createCanvas({ id: "wheel", width: 650, height: 730 });
  stage = new Stage("wheel");
  loader = load({
    elementName: "Wheel",
    src: WHEEL_RESOURCES,
    completeHandler: start
  });

  tickStage(stage);
};

function start() {
  buildScene();
  animateScene();
}

function buildScene() {
  container = new Container();
  container.wheel = new Bitmap(loader.getResult("wheel")).set({
    alpha: 0,
    x: 325,
    y: 375,
    regX: 325,
    regY: 325
  });
  container.wheel.cache(0, 0, 650, 650);
  container.wheel.snapToPixel = true;

  container.marker = new Bitmap(loader.getResult("marker")).set({
    alpha: 0,
    x: 300,
    y: -50
  });

  container.button = new Bitmap(loader.getResult("btn-spin")).set({
    alpha: 0,
    x: 150,
    y: 630,
    cursor: "pointer"
  });
  container.button.on("click", spinWheel);
  setOverEvents(container.button);

  container.addChild(container.wheel, container.marker, container.button);
  stage.addChild(container);
}

function animateScene() {
  fadeIn(container.wheel, 0);
  fadeIn(container.marker, 750);
  Tween.get(container.marker)
    .wait(750)
    .to({ y: 0 }, 100);
  fadeIn(container.button, 1000);
}

async function spinWheel() {
  if (spinning) return;
  spinning = true;

  container.wheel.rotation = 0;

  addBlur(container.wheel);
  wheelLoop = Tween.get(container.wheel, { loop: true }).to(
    { rotation: deg },
    400
  );
  //1 cat 2 bunny 3 ram 4 devil
  const data = await Api.get({ url: RANDOM_URL_API });

  setTimeout(() => {
    if (data && Number(data)) {
      stopWheel(Number(data));
    } else {
      stopWheel();
      alert("Error");
    }
  }, 1000);
}

function stopWheel(option) {
  removeLoop(wheelLoop);
  removeBlur(container.wheel);
  if (option) {
    spinning = false;
    Tween.get(container.wheel)
      .to({ rotation: deg - 50 + option * 90 }, 1000, Ease.backOut)
      .call(() => {
        Tween.get(container.wheel).to(
          { rotation: container.wheel.rotation + 5 },
          1000,
          Ease.elasticOut
        );
      });
  } else spinning = false;
}

export default Wheel;
