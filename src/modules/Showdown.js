import { Stage, Container, Bitmap, Tween } from "createjs";
import load from "../components/ResourceLoader";

import { SHOWDOWN_RESOURCES } from "../conf/constants";
import { createItem } from "../components/Helper";
import { createCanvas, tickStage } from "../components/Canvas";

let stage;
let loader;
let container;

const Showdown = () => {
  createCanvas({ id: "showdown", width: 1000, height: 430, classes: "header" });
  stage = new Stage("showdown");
  loader = load({
    elementName: "Showdown",
    src: SHOWDOWN_RESOURCES,
    completeHandler: start
  });
  tickStage(stage);
};

function start() {
  buildScene();
  animateScene();
}

function buildScene() {
  container = new Container().set({
    x: 90,
    y: 20,
    scaleX: 0.5,
    scaleY: 0.5
  });

  const bg = new Bitmap(loader.getResult("showdown-off"));
  bg.set({ x: 90, y: 20 });
  const item = new Bitmap(loader.getResult(name));

  createItem({ loader, container, name: "s", props: { x: -120, y: 5 } });
  createItem({ loader, container, name: "h", props: { x: 70, y: 5 } });
  createItem({ loader, container, name: "o1", props: { x: 380, y: 10 } });
  createItem({ loader, container, name: "w1", props: { x: 480, y: 10 } });
  createItem({ loader, container, name: "d", props: { x: 700, y: 10 } });
  createItem({ loader, container, name: "o2", props: { x: 860, y: 10 } });
  createItem({ loader, container, name: "w2", props: { x: 1038, y: 5 } });
  createItem({ loader, container, name: "n", props: { x: 1231, y: 10 } });
  createItem({ loader, container, name: "vegas", props: { x: 110, y: -20 } });
  createItem({ loader, container, name: "bolt", props: { x: 690, y: -100 } });
  createItem({ loader, container, name: "slots", props: { x: 870, y: -20 } });
  createItem({
    loader,
    container,
    name: "must_drop",
    props: { x: 240, y: 400, scale: 1.2 }
  });

  stage.addChild(bg, container);
}

function animateScene() {
  setTimeout(animateTopAndBottom, 500);
  setTimeout(animateShowdown, 1500);
}

function animateShowdown() {
  const speed = 150;
  const incrementTowait = 150;
  let timeToWait = 100;

  Tween.get(container.s).to({ alpha: 1 }, speed);
  Tween.get(container.h)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.o1)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.w1)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.d)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.o2)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.w2)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
  Tween.get(container.n)
    .wait((timeToWait += incrementTowait))
    .to({ alpha: 1 }, speed);
}

function animateTopAndBottom() {
  const speed = 100;
  const boltBlinkSpeed = 10;
  Tween.get(container.vegas, { loop: false })
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed)
    .wait(500)
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed)
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed);
  Tween.get(container.slots, { loop: false })
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed)
    .wait(500)
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed)
    .to({ alpha: 0 }, speed)
    .to({ alpha: 1 }, speed);

  setTimeout(() => {
    Tween.get(container.bolt, { loop: true })
      .to({ alpha: 0 }, boltBlinkSpeed)
      .to({ alpha: 1 }, boltBlinkSpeed)
      .wait(50);
  }, 1500);

  setTimeout(() => {
    Tween.get(container.must_drop, { loop: false })
      .to({ alpha: 0 }, speed)
      .to({ alpha: 1 }, speed)
      .wait(500)
      .to({ alpha: 0 }, speed)
      .to({ alpha: 1 }, speed)
      .to({ alpha: 0 }, speed)
      .to({ alpha: 1 }, speed);
  }, 2500);
}

export default Showdown;
