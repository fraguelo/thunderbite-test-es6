import { Tween, BlurFilter } from "createjs";

export function removeLoop(loop) {
  if (!loop) return;
  loop.paused = true;
  // loop.setPaused(true);
  loop = null;
}

export function fadeIn(obj, delay) {
  Tween.get(obj)
    .wait(delay)
    .to({ alpha: 1 }, 800);
}

export function addBlur(obj) {
  var blurFilter = new BlurFilter(5, 5, 1);
  obj.filters = [blurFilter];
  obj.cache(0, 0, 0 + obj.bitmapCache.width, 0 + obj.bitmapCache.height);
}

export function removeBlur(obj) {
  obj.filters = [];
  obj.cache(0, 0, obj.bitmapCache.width, obj.bitmapCache.height);
}

export function setOverEvents(button) {
  button.on("rollover", function() {
    button.alpha = 0.95;
  });
  button.on("rollout", function() {
    button.alpha = 1;
  });
}
