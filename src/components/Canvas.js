import { Ticker } from "createjs";
import { isMobile } from "./Helper";

export function tickStage(stage) {
  Ticker.addEventListener("tick", () => {
    stage.update();
  });
}

export function createCanvas({ id, classes = "", width, height }) {
  const div = document.createElement("div");
  div.setAttribute("class", classes);
  const canvas = document.createElement("canvas");
  canvas.setAttribute("id", id);
  canvas.setAttribute("width", width);
  canvas.setAttribute("height", height);
  div.appendChild(canvas);
  document.body.appendChild(div);
}

export function resizeCanvas() {
  if (isMobile() || window.innerWidth < 1000) {
    Array.from(document.getElementsByTagName("canvas")).forEach(
      canvas => (canvas.style.width = `${window.innerWidth}px`)
    );
  } else {
    Array.from(document.getElementsByTagName("canvas")).forEach(canvas =>
      canvas.removeAttribute("style")
    );
  }
}
