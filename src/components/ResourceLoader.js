import createjs from "createjs";
import { showLoader, hideLoader, progressHandler } from "./Loader";

export default function load({ completeHandler, src, elementName = "" }) {
  const loadCompleted = evt => {
    completeHandler(evt);
    hideLoader();
  };

  const resourceLoader = new createjs.LoadQueue(false);

  resourceLoader.loadManifest({ src, type: "manifest" });
  resourceLoader.addEventListener("complete", loadCompleted);

  resourceLoader.on("progress", progressHandler);
  showLoader({ elementName });
  return resourceLoader;
}
