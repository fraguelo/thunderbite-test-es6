import { Bitmap, Tween, BlurFilter } from "createjs";

export function createItem({ container, name, props, loader }) {
  var item = new Bitmap(loader.getResult(name)).set({
    x: props.x,
    y: props.y,
    alpha: 0
  });

  if (props && props.scale)
    item.set({ scaleX: props.scale, scaleY: props.scale });

  container[name] = item;
  container.addChild(item);
  return item;
}

export function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
export function isMobile() {
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    return true;
  }
  return false;
}
