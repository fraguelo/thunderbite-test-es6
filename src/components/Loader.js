let loader, loaderText;
const queue = [];

function createLoader() {
  loader = document.createElement("div");
  loaderText = document.createElement("div");
  loader.setAttribute("id", "loader");
  loader.appendChild(loaderText);
  document.body.appendChild(loader);
}

function progressHandler(event) {
    loaderText.textContent = `Loading ${queue.length > 0 && queue[0]} ${Math.floor(
    event.progress * 100
  )}%`;
}

function showLoader({elementName}) {
  !loader && createLoader();
  queue.push(elementName);
  loader.removeAttribute("class");
  loader.setAttribute("class", "fadeIn");
}

function hideLoader() {
  queue.shift();
  if (queue.length > 0) return;
  loader.removeAttribute("class");
  loader.setAttribute("class", "fadeOut");
}

export { progressHandler, showLoader, hideLoader };
