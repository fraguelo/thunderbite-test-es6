export const SHOWDOWN_RESOURCES = './src/assets/json/showdown.json';
export const WHEEL_RESOURCES = './src/assets/json/wheel.json';
export const RANDOM_URL_API =
  "https://www.random.org/integers/?num=1&min=1&max=4&col=1&base=10&format=plain&rnd=new";
